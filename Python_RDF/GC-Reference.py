
import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
#from rdflib.namespace import XSD  # @prefix xsd
#from rdflib.namespace import Namespace, NamespaceManager
# from rdflib.namespace import rxn, bp  # @prefix hpa & wp & nif
from rdflib import URIRef,Literal
from rdflib.namespace import XSD  # @prefix xsd

# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
xml = rdflib.Namespace("<http://www.w3.org/2001/XMLSchema#string#>")
# CSVファイルを読み込む
f = open('GC-PublicationXref.csv','rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]

    if row[0] != "PublicationXref":
        # 一つ目のトリプル
        gc_rxn= URIRef(rxn+ col_A)
        bp_id = Literal(col_B, datatype=XSD.string)
        ref_link = URIRef(col_C)
        bp_type = URIRef(bp + col_D)
        bp_db = Literal(col_E, datatype=XSD.string)

        if col_B !="":
            g.add((gc_rxn, bp.id, bp_id))
        if col_C != "":
            g.add((gc_rxn, RDFS.seeAlso, ref_link))
        g.add((gc_rxn, RDF.type, bp_type))
        g.add((gc_rxn, bp.db, bp_db))


g.serialize(destination="GC_Publication_RDF_New.ttl", format='turtle')