
import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
#from rdflib.namespace import XSD  # @prefix xsd
#from rdflib.namespace import Namespace, NamespaceManager
# from rdflib.namespace import rxn, bp  # @prefix hpa & wp & nif
from rdflib import URIRef

# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSVファイルを読み込む
f = open('GC-Reaction.csv','rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]

    if row[0] != "BiochemicalReaction":
        # 一つ目のトリプル
        gc_rxn= URIRef(rxn+ col_A)
        sm_left = URIRef(rxn + col_B)
        sm_right = URIRef(rxn + col_C)
        sm_stoichiometry = URIRef(rxn + col_D)
        sm_type = URIRef(bp + col_E)
        if col_B != "":
            g.add((gc_rxn, bp.left, sm_left))
        if col_C != "":
            g.add((gc_rxn, bp.right, sm_right))
        if col_E != "":
            g.add((gc_rxn, RDF.type, sm_type))
        #g.add((gc_rxn, bp.right, sm_right))
        g.add((gc_rxn, bp.participantStoichiometry, sm_stoichiometry))

g.serialize(destination="GC_RXN_RDF_New.ttl", format='turtle')
