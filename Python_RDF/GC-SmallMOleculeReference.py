
import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
#from rdflib.namespace import XSD  # @prefix xsd
#from rdflib.namespace import Namespace, NamespaceManager
# from rdflib.namespace import rxn, bp  # @prefix hpa & wp & nif
from rdflib import URIRef, Literal, XSD

# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSVファイルを読み込む
f = open('GC-SmallmoleculeReference.csv','rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]

    if row[0] != "SmallMoleculeReference":
        # 一つ目のトリプル
        ref_rxn= URIRef(rxn + col_A)
        ref_uni = URIRef(rxn + col_B)
        ref_type = URIRef(bp + col_C)

#g.add((path_rxn, bp.pathwayComponent, path_component_2))
        g.add((ref_rxn, bp.xref, ref_uni))
        g.add((ref_rxn, RDF.type, ref_type))

g.serialize(destination="GC_SmallMoleculeReference_RDF.ttl", format='turtle')