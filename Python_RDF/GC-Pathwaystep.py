
import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
#from rdflib.namespace import XSD  # @prefix xsd
#from rdflib.namespace import Namespace, NamespaceManager
# from rdflib.namespace import rxn, bp  # @prefix hpa & wp & nif
from rdflib import URIRef
from rdflib import URIRef, BNode, Literal,XSD

# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSVファイルを読み込む
f = open('GC-PathwayStep.csv','rt')

dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]
    col_F = row[5]

    if row[0] != "PathwayStep":
        # 一つ目のトリプル
        pws_rxn= URIRef(rxn + col_A)
        pws_nextstep = URIRef(rxn + col_B)
        pws_stepconversion = URIRef(rxn + col_C)
        pws_direction = Literal(col_D, datatype=XSD.string)
        pws_process = URIRef(rxn + col_E)
        pws_type= URIRef(bp + col_F)

        if col_B != "":
            g.add((pws_rxn, bp.nextStep, pws_nextstep))
        g.add((pws_rxn, bp.stepConversion, pws_stepconversion))
        g.add((pws_rxn, bp.stepDirection, pws_direction))
        g.add((pws_rxn, bp.stepProcess, pws_process))
        g.add((pws_rxn, RDF.type, pws_type))

g.serialize(destination="GC_PathwayStep_RDF.ttl", format='turtle')