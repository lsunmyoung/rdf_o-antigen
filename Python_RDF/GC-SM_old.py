import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
from rdflib import URIRef
from rdflib import URIRef, BNode, Literal, XSD

# ontology defining
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSV file open
f = open('GC-SmallMolecule.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

# making graph
g = Graph()

for row in dataReader:
    # Instances of SmallMolecule Class
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]


    if row[0] != "SmallMolecule":


        gc_sm= URIRef(rxn + col_A)
        sm_enti_Ref = URIRef(rxn + col_B)
        sm_name = Literal(col_C, datatype=XSD.string)
        sm_type = URIRef(bp + col_D)
        sm_xref = URIRef(rxn + col_E)

        g.add((gc_sm, bp.entityReference, sm_enti_Ref))
        g.add((gc_sm, bp.displayName, sm_name))
        g.add((gc_sm, RDF.type, sm_type))
        g.add((gc_sm, bp.xref, sm_xref))

# data output

g.serialize(destination="GC_SmallMolecule_RDF_New.ttl", format='turtle')
