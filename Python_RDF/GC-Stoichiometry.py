import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
from rdflib import URIRef
from rdflib import URIRef, BNode, Literal, XSD

# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSVファイルを読み込む
f = open('GC-Stoichiometry.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Instances of SmallMolecule Class
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]


    if row[0] != "Stoichiometry":
        # 一つ目のトリプル

        gc_sto= URIRef(rxn + col_A)
        sto_type = URIRef(bp + col_B)
        sto_entity = URIRef(rxn + col_C)
        sto_coef = Literal(col_D, datatype=XSD.integer)

        g.add((gc_sto, RDF.type, sto_type))
        g.add((gc_sto, bp.physicalEntity, sto_entity))
        g.add((gc_sto, bp.stoichiometryCoefficient, sto_coef))

        # g.add((ensembl, RDFS.label, genesymbol))
        # g.add((ensembl, RDFS.seeAlso, ensembl_ts_uri))

g.serialize(destination="GC_Stoichiometry_RDF_New.ttl", format='turtle')