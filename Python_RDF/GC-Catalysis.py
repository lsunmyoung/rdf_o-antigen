import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
from rdflib import URIRef, Literal, XSD
#from rdflib.namespace import XSD
# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")

# CSVファイルを読み込む
f = open('GC-catalysis.csv', 'rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Instances of SmallMolecule Class
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]
#print("row[0]", row[0])

    if row[0] != "Catalysis":
        #print("into the if")
        gc_cat= URIRef(rxn + col_A)
        cat_controller = URIRef (rxn + col_B)
        cat_controlled = URIRef (rxn +col_C)
        cat_cotroltype = Literal(col_D, datatype=XSD.string) #datatype= XSD.string))
        cat_type= URIRef(bp + col_E)

# name_string = Literal(col_D)
        g.add((gc_cat, bp.controller, cat_controller))
        g.add((gc_cat, bp.controlled, cat_controlled))
        g.add((gc_cat, bp.controlType, cat_cotroltype))
        g.add((gc_cat, RDF.type, cat_type))

        # g.add((ensembl, RDFS.label, genesymbol))
        # g.add((ensembl, RDFS.seeAlso, ensembl_ts_uri))

g.serialize(destination="GC_Catalysis_RDF.ttl", format='turtle')