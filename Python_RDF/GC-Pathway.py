
import csv
# import requests
# import sys
import rdflib
from rdflib import Graph
from rdflib import Namespace
from rdflib.namespace import RDF, RDFS  # @prefix rdf & rdfs
#from rdflib.namespace import XSD  # @prefix xsd
#from rdflib.namespace import Namespace, NamespaceManager
# from rdflib.namespace import rxn, bp  # @prefix hpa & wp & nif
from rdflib import URIRef, Literal, XSD

# ontologyを定義
rxn = rdflib.Namespace("http://glycosmos.org/biopax/1/562#")
bp = rdflib.Namespace("http://www.biopax.org/release/biopax-level3.owl#")
owl = rdflib.Namespace("http://www.w3.org/2002/07/owl#")
# CSVファイルを読み込む
f = open('GC-Pathway.csv','rt')
dataReader = csv.reader(f, delimiter=',')

g = Graph()

# 必要なデータをとってくる
for row in dataReader:
    # Class row of Instances
    col_A = row[0]
    col_B = row[1]
    col_C = row[2]
    col_D = row[3]
    col_E = row[4]
    col_F = row[5]
    col_G = row[6]

    if row[0] != "Pathway":
        # 一つ目のトリプル
        path_rxn= URIRef(rxn + col_A)
        path_component_1 = URIRef(rxn + col_B)
        path_component_2 = URIRef(rxn + col_C)
        path_order = URIRef(rxn + col_D)
        path_ref = URIRef(bp + col_E)
        path_type = URIRef(bp + col_F)
        path_displayName = Literal(col_G, datatype=XSD.string)


        #g.add((path_rxn, bp.pathwayComponent, path_component_2))


        if col_B !="":
            g.add((path_rxn, bp.pathwayComponent, path_component_1))
        if col_C !="":
            g.add((path_rxn, bp.pathwayComponent, path_component_2))
        if col_D !="":
            g.add((path_rxn, bp.pathwayOrder, path_order))
        if col_E != "":
            g.add((path_rxn, bp.xref, path_ref))
        if col_F != "":
            g.add((path_rxn, RDF.type, path_type))
        if col_G != "":
            g.add((path_rxn, bp.displayName, path_displayName))


g.serialize(destination="GC_Pathway_RDF_New.ttl", format='turtle')